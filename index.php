<?php
require_once 'config.php';
date_default_timezone_set('Europe/Paris');
$conf_ini = parse_ini_file('config.ini', true);
$asso_db = new asso($conf_ini['assos']);
$origin_db = new origin($conf_ini['origin']);
$destination_db = new destination($conf_ini['destination']);
$utils = new utils($conf_ini['hash']['key']);
write_logs('start');
$origin_users = $origin_db->get_users();
foreach ($origin_users as $user) {
    $hash_id_user = $utils->anonymous_id($user['id']);
    if(!$asso_db->check_id_exist($hash_id_user)){
        $user_anonymised = $utils->anonymous_user($user['email']);
        $user['email'] = $user_anonymised['mail'];
        $user['username'] = $user_anonymised['username'];
        $user['firstname'] = $user_anonymised['firstname'];
        $user['lastname'] = $user_anonymised['lastname'];
        $first_line_query = '';
        $scd_line_query = '';
        foreach ($user as $field_name => $field_value) {
            if(gettype($field_name) != 'integer' and $field_name != 'id'){

                $first_line_query .= $field_name . ',';
                $scd_line_query .= '"' . $field_value . '",';
            }
        }
        $destination_db->add_user($first_line_query, $scd_line_query);
        $last_id_user = $destination_db->last_insert_id();
        $asso_db->add_asso($hash_id_user, $last_id_user);
    }
}

$exclude_tables = array('logstore_xapi_failed_log', 'logstore_xapi_log', 'config', 'config_log', 'config_plugins', 'user', 'task_scheduled');
$user_ref = 'userid';
// récupère les tables des deux bases de données origine et destinations
$dest_tables = $destination_db->get_tables();
$origin_tables = $origin_db->get_tables();
foreach ($origin_tables as $tbl_origin) {
    echo $tbl_origin . PHP_EOL;
    $table_name_no_prefix = $origin_db->remove_prefix($tbl_origin);
    if(!in_array($table_name_no_prefix, $exclude_tables)){
        $name_table_dest = $destination_db->add_prefix($table_name_no_prefix);
        if(in_array($name_table_dest, $dest_tables)){
            $count_origin_table = $origin_db->count_table($tbl_origin);
            $count_dest_table = $destination_db->count_table($name_table_dest);
            if($count_origin_table != $count_dest_table){
                // vide la table de destination
                if($destination_db->empty_table($name_table_dest) != false){
                    $colunm_current_table = $origin_db->describe_table($tbl_origin);
                    $current_entries = $origin_db->get_entry_table($tbl_origin);
                    foreach ($current_entries as $entry) {
                        if(in_array($user_ref, $colunm_current_table) && $entry[$user_ref] > 0){
                            $real_user_hash = $utils->anonymous_id($entry[$user_ref]);
                            if($asso_db->check_id_exist($real_user_hash)){
                                $entry[$user_ref] = $asso_db->get_asso_id($real_user_hash);
                            }else{
                                continue;
                            }
                            
                        }
                        $first_part_query = '';
                        $scd_part_query = '';
                        $column_table_dest = $destination_db->describe_table($name_table_dest);

                        foreach ($column_table_dest as $entry_key) {
                            if(array_key_exists($entry_key, $entry)){
                                $entry_value = $entry[$entry_key];
                                if($entry_value != null){
                                    $first_part_query .= $entry_key . ',';
                                    if(strpos($entry_value, '"') !== false && strpos($entry_value, '\\') !== false){
                                        $scd_part_query .= '"' . $entry_value . '",';
                                    }else{
                                        $temp_entry_value = str_replace('\'', ' ', $entry_value);
                                        $temp_entry_value = str_replace('\\', '\\\\', $temp_entry_value);
                                        if($name_table_dest == 'mdl_logstore_standard_log' && $entry_key == 'eventname'){
                                            var_dump($temp_entry_value);
                                        }
                                        $scd_part_query .= '\'' . $temp_entry_value . '\',';
                                    }
                                    

                                }else{
                                    $cant_null = $utils->cant_null_entry($destination_db, $name_table_dest, $entry_key);
                                    if($cant_null != false){
                                        $first_part_query .= $cant_null['first'];
                                        $scd_part_query .= $cant_null['scd'];

                                    }
                                }
                            }else{
                                $cant_null = $utils->cant_null_entry($destination_db, $name_table_dest, $entry_key);
                                if($cant_null != false){
                                    $first_part_query .= $cant_null['first'];
                                    $scd_part_query .= $cant_null['scd'];

                                }
                            }
                        }
                        $add_entry = $destination_db->add_entry($first_part_query, $scd_part_query, $name_table_dest);
                        if($add_entry['result'] == false){
                            write_logs('erreur d\'enregistrement de cettte requête : ' . $add_entry['query']);
                        }
                    }
                }else{
                    write_logs('erreur effacement table : ' . $name_table_dest);
                }
            }
        }
    }
}

if($asso_db->last_log_exist()){
    $last_log = $asso_db->get_last_log();
    $destination_db->prepare_log($last_log->timecreated);
    $timecreated = $destination_db->get_last_log();
    $asso_db->update_last_log($last_log->id, $timecreated);
}else{
    $destination_db->prepare_log();
    $timecreated = $destination_db->get_last_log();
    $asso_db->add_last_log($timecreated);
}
write_logs('end');
function write_logs($message){
    $base_dir_log = 'logs/';
    $name_file_log = date('d-m-Y') . '.log';
    $path_log_file = $base_dir_log . $name_file_log;
    if(!file_exists($base_dir_log)){
        mkdir($base_dir_log);
    }
    $log_file  = fopen($path_log_file, 'a+');
    $log_message = '[' . date('H:i:s') . '] ' . $message . PHP_EOL;
    fwrite($log_file, $log_message);
}