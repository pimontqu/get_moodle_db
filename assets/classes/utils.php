<?php

class utils{
    private $hash_key;

    public function __construct($hash_key){
        $this->hash_key = $hash_key;
    }

    public function anonymous_user($mail){
        $hash_dict = array();
        $hash = hash_hmac('sha256', $mail, $this->hash_key);
        $hash_dict['mail'] = $hash . '@mail-lille.fr';
        $hash_dict['username'] = substr($hash, 0, 10);
        $hash_dict['firstname'] = substr($hash_dict['username'], 0, 5);
        $hash_dict['lastname'] = substr($hash_dict['username'], 5);
        return $hash_dict;
    }

    public function anonymous_id($id){
        $hash_id = hash_hmac('sha256', $id, $this->hash_key);
        return $hash_id;
    }


    public function cant_null_entry($db, $table_name, $column_name){
        $desc_column = $db->describe_column($table_name, $column_name);
        if($desc_column['Null'] == 'NO' && $desc_column['Default'] == null){
            $num_type = array('INTEGER', 'INT', 'SMALLINT', 'TINYINT', 'MEDIUMINT', 'BIGINT', 'DECIMAL', 'NUMERIC', 'FLOAT', 'DOUBLE');
            $query_parts = array();
            $query_parts['first'] = $column_name . ',';
            $field_type = explode('(', $desc_column['Type'])[0];
            if(in_array(strtoupper($field_type), $num_type)){
                $query_parts['scd'] = '"0",';
            }else{
                $query_parts['scd'] = '"",';
            }
            return $query_parts;
        }else{
            return false;
        }
    }

    public function add_module_table(origin $origin_db){
        $module_tables = $origin_db->get_module_name();
        $all_table = array();
        foreach ($module_tables as $table) {
            array_push($all_table, $table->name);
        }
        return $all_table;
    }
}