<?php

class destination extends db
{
    public function __construct($info_db)
    {
        parent::__construct($info_db);
        $this->prefix_table = $info_db['prefix'];
    }

    public function add_user($first_line, $scd_line){
        $query = 'INSERT INTO ' . $this->prefix_table . 'user(' . substr($first_line, 0, -1) . ') VALUES(' . substr($scd_line, 0, -1) . ')';
        return $this->dbConnect->query($query);
    }

    public function add_entry($query_parts, $table_name){
        $query = 'INSERT INTO ' . $this->add_prefix($table_name) . '(' . $query_parts['fields'] . ') VALUES(' . $query_parts['values'] . ')';
        $result = $this->dbConnect->query($query);
        return array('query' => $query, 'result' => $result);
    }

    public function get_last_log(){
        $query = 'SELECT timecreated FROM ' . $this->prefix_table . 'logstore_xapi_log ORDER BY timecreated DESC LIMIt 1';
        $last_log = $this->dbConnect->query($query);
        return $last_log->fetch(PDO::FETCH_OBJ)->timecreated; 
    }

    public function prepare_log($timecreated = 0){
        $query = 'INSERT INTO ' . $this->prefix_table . 'logstore_xapi_log SELECT * FROM ' . $this->prefix_table . 'logstore_standard_log WHERE timecreated > :timecreated';
        $prepare_log = $this->dbConnect->prepare($query);
        $prepare_log->bindValue(':timecreated', $timecreated, PDO::PARAM_INT);
        return  $prepare_log->execute();
    }

    public function check_site_exit(){
        $query = 'SELECT COUNT(*) as exist FROM ' . $this->prefix_table . 'course WHERE format = "site"';
        $get_site = $this->dbConnect->query($query);
        $exist = (int)$get_site->fetch(PDO::FETCH_OBJ)->exist;
        return $exist === 0 ? false : true;
    }

    public function check_course_exit($id){
        $table_name = $this->add_prefix('course');
        $query = 'SELECT COUNT(*) as exist FROM ' . $table_name . ' WHERE id = :id';
        $get_course = $this->dbConnect->prepare($query);
        $get_course->bindValue(':id', $id, PDO::PARAM_INT);
        $get_course->execute();
        return (int)$get_course->fetch(PDO::FETCH_OBJ)->exist === 1 ? true : false;
    }

    public function check_attemps_quiz_exit($id){
        $table_name = $this->add_prefix('quiz_attempts');
        $query = 'SELECT COUNT(*) as exist FROM ' . $table_name . ' WHERE id = :id';
        $get_attemp = $this->dbConnect->prepare($query);
        $get_attemp->bindValue(':id', $id, PDO::PARAM_INT);
        $get_attemp->execute();
        return (int)$get_attemp(PDO::FETCH_OBJ)->exist ? true : false;
    }

    public function check_quiz_exist($id){
        $table_name = $this->add_prefix('quiz');
        $query = 'SELECT COUNT(*) as exist FROM ' . $table_name . ' WHERE id = :id';
        $get_quiz = $this->dbConnect->prepare($query);
        $get_quiz->bindValue(':id', $id, PDO::PARAM_INT);
        $get_quiz->execute();
        return (int)$get_quiz->fetch(PDO::FETCH_OBJ)->exist ? true : false;
    }

    public function module_course_exist($id){
        $table_name = $this->add_prefix('course_modules');
        $query = 'SELECT COUNT(*) as exist FROM ' . $table_name . ' WHERE id = :id';
        $get_module->bindValue(':id', $id, PDO::PARAM_INT);
        $get_module->execute();
        return (int)$get_module->fetch(PDO::FETCH_OBJ)->exist ? true : false;
    }

    public function quiz_slot_exit($id){
        $table_name = $this->add_prefix('quiz_slots');
        $query = 'SELECT COUNT(*) as exist FROM ' . $table_name . ' WHERE id = :id';
        $get_slot->bindValue(':id', $id, PDO::PARAM_INT);
        $get_slot->execute();
        return (int)$get_slot->fetch(PDO::FETCH_OBJ)->exist ? true : false;
    }

    public function question_exist($id){
        $table_name = $this->add_prefix('question');
        $query = 'SELECT COUNT(*) as exist FROM ' . $table_name . ' WHERE id = :id';
        $get_question->bindValue(':id', $id, PDO::PARAM_INT);
        $get_question->execute();
        return (int)$get_question->fetch(PDO::FETCH_OBJ)->exist ? true : false;
    }

    public function grade_items_exist($id_quiz){
        $table_name = $this->add_prefix('grade_items');
        $query = 'SELECT COUNT(*) as exist FROM ' . $table_name . ' WHERE itemmodule = "quiz" AND iteminstance = :id_quiz';
        $grade_items->bindValue(':id_quiz', $id_quiz, PDO::PARAM_INT);
        $grade_items->execute();
        return (int)$grade_items->fetch(PDO::FETCH_OBJ)->exist ? true : false;
    }

    public function module_exist($id, $table){
        $table_name = $this->add_prefix($table);
        $query = 'SELECT COUNT(*) FROM ' . $table_name . ' WHERE id = :id';
        $module = $this->dbConnect->prepare($query);
        $module->bindValue(':id', $id, PDO::PARAM_INT);
        $module->execute();
        return (int)$module->fetch(PDO::FETCH_OBJ)->exist ? true : false;
    }
}