<?php

/**
 * classe mère
 */
class db
{
    protected $host;
    protected $pwd;
    protected $login;
    protected $db;
    protected $dbConnect;
    public $prefix_table;

    /**
     * Se connecte à la base de données
     */
    public function __construct($info_db) {
        $this->host = $info_db['host'];
        $this->db = $info_db['db'];
        $this->login = $info_db['user'];
        $this->pwd = $info_db['pwd'];
        try {
            $this->dbConnect = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->db . ';charset=utf8', $this->login, $this->pwd);
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }
    /**
     * récupère les tables de la base de donnée
     */
    public function get_tables(){
        $tables = $this->dbConnect->query('SHOW TABLES');
        return $tables->fetchAll(PDO::FETCH_COLUMN);

    }

    /**
     * rècupère les champs de la table
     */

    public function describe_table($table){
        $column = $this->dbConnect->query('DESCRIBE ' . $table);
        //var_dump($column);
        return $column->fetchAll(PDO::FETCH_COLUMN);
    }

    public function describe_column($table, $column_name)
    {   
        $column = $this->dbConnect->query('SHOW COLUMNS FROM ' . $table . ' WHERE Field="' . $column_name . '"');
        return $column->fetch(PDO::FETCH_ASSOC);
    }
    /**
     * met le prefixe de la table
     */
    public function add_prefix($table_name){
        return $this->prefix_table . $table_name;
    }
    /**
     * enlève le prefix de la table
     */
    public function remove_prefix($table_name){
        return substr($table_name, strlen($this->prefix_table));
    }

    /**
     * récupère le dernier id enregistré
     */
    public function last_insert_id() {
        return $this->dbConnect->lastInsertId();
    }

    public function count_table($table_name){
        $query = 'SELECT COUNT(*) FROM ' . $table_name;
        $count_table = $this->dbConnect->query($query);
        return $count_table->fetch(PDO::FETCH_COLUMN);
    }

    public function empty_table($table_name){
        $query = 'DELETE FROM ' . $table_name;
        $result =  $this->dbConnect->query($query);
        return $result;
    }


}
