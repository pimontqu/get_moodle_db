<?php

class asso extends db
{
    public function __construct($info_db)
    {
        parent::__construct($info_db);
    }

    public function check_id_exist($hash_real){
        $get_id = $this->dbConnect->prepare('SELECT COUNT(*) as exist FROM assos_id WHERE `hash_real`=:realid');
        $get_id->bindValue(':realid', $hash_real, PDO::PARAM_INT);
        $get_id->execute();
        $exist = (int)$get_id->fetch(PDO::FETCH_OBJ)->exist == 0? false : true;
        return $exist;
    }

    public function get_asso_id($hash_real){
        $get_id = $this->dbConnect->prepare('SELECT asso FROM assos_id WHERE `hash_real`=:realid');
        $get_id->bindValue(':realid', $hash_real, PDO::PARAM_INT);
        $get_id->execute();
        return $get_id->fetch(PDO::FETCH_OBJ)->asso;
    }

    public function add_asso($hash_real, $asso_id){
        $add_asso = $this->dbConnect->prepare('INSERT INTO assos_id(`hash_real`, `asso`) VALUES (:realid, :assoid)');
        $add_asso->bindValue(':realid', $hash_real, PDO::PARAM_INT);
        $add_asso->bindValue(':assoid', $asso_id, PDO::PARAM_INT);
        return $add_asso->execute();
    }

    public function add_last_log($created_time){
        $query = 'INSERT INTO asso_utils(last_log) VALUES(:last_log)';
        $add_last_log = $this->dbConnect->prepare($query);
        $add_last_log->bindValue(':last_log', $created_time, PDO::PARAM_INT);
        return $add_last_log->execute();
    }

    public function update_last_log($created_time){
        $query = 'UPDATE asso_utils SET last_log = :last_log WHERE last_log IS NOT NULL';
        $update_last_log = $this->dbConnect->prepare($query);
        $update_last_log->bindValue(':last_log', $created_time, PDO::PARAM_INT);
        return $update_last_log->execute();
    }

    public function get_last_log(){
        $query = 'SELECT last_log FROM asso_utils WHERE last_log IS NOT NULL LIMIT 1';
        $get_last_log = $this->dbConnect->query($query);
        return $get_last_log->fetch(PDO::FETCH_OBJ)->last_log;
    }

    public function is_first_exe(){
        $query = 'SELECT COUNT(*) as is_first_exe FROM asso_utils WHERE with_first_log = 1';
        $is_first_exe = $this->dbConnect->query($query);
        return (int)$is_first_exe->fetch(PDO::FETCH_OBJ)->is_first_exe === 0 ? false : true;

    }

    public function make_first_exe(){
        $query = 'INSERT INTO asso_utils(with_first_log) VALUES(1)';
        return $this->dbConnect->query($query);
    }
}