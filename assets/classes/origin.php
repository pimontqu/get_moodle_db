<?php

class origin extends db
{
    
    public function __construct($info_db)
    {
        parent::__construct($info_db);
        $this->prefix_table = $info_db['prefix'];
    }

    public function get_users(){
        $query = 'SELECT * FROM ' . $this->prefix_table . 'user';
        $get_user = $this->dbConnect->query($query);
        return $get_user->fetchAll();
    }

    public function get_entry_table($table){
        $query = 'SELECT * FROM ' . $table;
        $get_entry = $this->dbConnect->query($query);
        return $get_entry->fetchAll(PDO::FETCH_ASSOC);
    }

    public function get_logs($timecreated, $end_queries){
        $query = 'SELECT * FROM ' . $this->prefix_table . 'logstore_standard_log WHERE timecreated > :timecreated AND ' . $end_queries;
        $get_logs = $this->dbConnect->prepare($query);
        $get_logs->bindValue(':timecreated', $timecreated, PDO::PARAM_INT);
        $get_logs->execute();
        return $get_logs->fetchAll(PDO::FETCH_ASSOC);
    }
    // récupère les infos du site moodle
    public function get_site(){
        $query = 'SELECT * FROM ' . $this->prefix_table . 'course WHERE format = "site"';
        $get_site = $this->dbConnect->query($query);
        return $get_site->fetch(PDO::FETCH_ASSOC);
    }

    // rècupère les infos d'un cours
    public function get_course($id){
        $table_name = $this->prefix_table . 'course'; 
        $query = 'SELECT * FROM ' . $table_name . ' WHERE id = :id';
        $get_course = $this->dbConnect->prepare($query);
        $get_course->bindValue(':id', $id, PDO::PARAM_STR);
        $get_course->execute();
        return $get_course->fetch(PDO::FETCH_ASSOC);
    }

    public function get_attemp_quiz($id){
        $table_name = $this->add_prefix('quiz_attempts');
        $query = 'SELECT * FROM ' . $table_name . ' WHERE id = :id';
        $get_attemp = $this->dbConnect->prepare($query);
        $get_attemp->bindValue(':id', $id, PDO::PARAM_INT);
        $get_attemp->execute();
        return $get_attemp->fetch(PDO::FETCH_ASSOC);
    }

    public function get_quiz($id){
        $table_name = $this->add_prefix('quiz');
        $query = 'SELECT * FROM ' . $table_name . ' WHERE id = :id';
        $get_quiz = $this->dbConnect->prepare($query);
        $get_quiz->bindValue(':id', $id, PDO::PARAM_INT);
        $get_quiz->execute();
        return $get_quiz->fetch(PDO::FETCH_ASSOC);

    }

    public function get_course_module($id){
        $table_name = $this->add_prefix('course_modules');
        $query = 'SELECT * FROM ' . $table_name . ' WHERE id = :id';
        $get_module = $this->dbConnect->prepare($query);
        $get_module->bindValue(':id', $id, PDO::PARAM_INT);
        $get_module->execute();
        return $get_module->fetch(PDO::FETCH_ASSOC);
    }

    public function get_quiz_slot($id_quiz){
        $table_name = $this->add_prefix('quiz_slots');
        $query = 'SELECT * FROM ' .  $table_name . ' WHERE quizid = :id_quiz';
        $get_slots = $this->dbConnect->prepare($query);
        $get_slots->bindValue(':id_quiz', $id_quiz, PDO::PARAM_INT);
        $get_slots->execute();
        return $get_slots->fetchAll(PDO::FETCH_ASSOC);
    }

    public function get_question($id){
        $table_name = $this->add_prefix('question');
        $query = 'SELECT * FROM ' .  $table_name . ' WHERE id = :id';
        $get_question = $this->dbConnect->prepare($query);
        $get_question->bindValue(':id', $id, PDO::PARAM_INT);
        $get_question->execute();
        return $get_question->fetch(PDO::FETCH_ASSOC);
    }

    public function get_question_attemps($unique_id){
        $table_name = $this->add_prefix('question_attempts');
        $query = 'SELECT * FROM ' . $table_name . ' WHERE questionusageid = :questionusageid';
        $get_attemps = $this->dbConnect->prepare($query);
        $get_attemps->bindValue(':questionusageid', $unique_id, PDO::PARAM_INT);
        $get_attemps->execute();
        return $get_attemps->fetchAll(PDO::FETCH_ASSOC);
    }

    public function get_question_usage($id){
        $table_name = $this->add_prefix('question_usages');
        $query = 'SELECT * FROM ' . $table_name . ' WHERE id = :id';
        $get_question_usage = $this->dbConnect->prepare($query);
        $get_question_usage->bindValue(':id', $id, PDO::PARAM_INT);
        $get_question_usage->execute();
        return $get_question_usage->fetch(PDO::FETCH_ASSOC);
    }

    public function get_grade_items($id_quiz){
        $table_name = $this->add_prefix('grade_items');
        $query = 'SELECT * FROM ' . $table_name . ' WHERE itemmodule = "quiz" AND iteminstance = :id_quiz';
        $get_grade_items = $this->dbConnect->prepare($query);
        $get_grade_items->bindValue(':id_quiz', $id_quiz, PDO::PARAM_INT);
        $get_grade_items->execute();
        return $get_grade_items->fetch(PDO::FETCH_ASSOC);
    }

    public function get_grade_grades($id_user, $id_grade_items){
        $table_name = $this->add_prefix('grade_grades');
        $query = 'SELECT * FROM ' . $table_name . ' WHERE userid = :userid AND itemid = :itemid';
        $get_grade_grades = $this->dbConnect->prepare($query);
        $get_grade_grades->bindValue(':userid', $id_user, PDO::PARAM_INT);
        $get_grade_grades->bindValue(':itemid', $id_grade_items, PDO::PARAM_INT);
        $get_grade_grades->execute();
        return $get_grade_grades->fetch(PDO::FETCH_ASSOC);
    }

    public function get_module_name(){
        $table_name = $this->add_prefix('modules');
        $query = 'SELECT `name` FROM ' . $table_name;
        $get_module = $this->dbConnect->query($query);
        return $get_module->fetchAll(PDO::FETCH_OBJ);
    }

    public function get_module_in_course($id_course){
        $table_name = $this->add_prefix('course_modules');
        $query = 'SELECT * FROM ' . $table_name . ' WHERE course = :id_course';
        $get_module = $this->dbConnect->prepare($query);
        $get_module->bindValue(':id_course', $id_course, PDO::PARAM_INT);
        $get_module->execute();
        return $get_module->fetchAll(PDO::FETCH_ASSOC);
    }

    public function get_module_instance($id_instance, $table){
        $table_name = $this->add_prefix($table);
        $query = 'SELECT * FROM ' . $table_name . ' WHERE id = :id_instance';
        $get_instance = $this->dbConnect->prepare($query);
        $get_instance->bindValue(':id_instance', $id_instance, PDO::PARAM_INT);
        $get_instance->execute();
        return $get_instance->fetchAll(PDO::FETCH_ASSOC);
    }

    public function get_module_table_name($id){
        $table_name = $this->add_prefix('modules');
        $query = 'SELECT `name` FROM ' . $table_name . ' WHERE id = :id';
        $get_name = $this->dbConnect->prepare($query);
        $get_name->bindValue(':id', $id, PDO::PARAM_INT);
        $get_name->execute();
        return $get_name->fetch(PDO::FETCH_ASSOC)['name'];
    }
    
}