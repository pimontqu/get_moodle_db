<?php

class db_helper{
    public function change_user_ref(array $current_log, utils $utils, asso $asso_db){
        $user_ref = 'userid';
        $user_ref_bis = 'relateduserid';
        $created_by = 'createdby';
        if(key_exists($user_ref, $current_log)){
            $id_hash = $utils->anonymous_id($current_log[$user_ref]);
            $current_log[$user_ref] = $asso_db->get_asso_id($id_hash);
            if(key_exists($user_ref_bis, $current_log) && $current_log[$user_ref_bis] != null){
                $current_log[$user_ref_bis] = $current_log[$user_ref];
            }
        }
        if(key_exists($created_by, $current_log)){
            $id_hash = $utils->anonymous_id($current_log[$created_by]);
            $current_log[$created_by] = $asso_db->get_asso_id($id_hash);
        }
        return $current_log;
    }
    
    public function create_query(array $current_log){
        $current_log = str_replace('\\', '\\\\', $current_log);
        $query_parts = array('fields' => '', 'values' => '');
        foreach ($current_log as $name_field => $value_field) {
            $query_parts['fields'] .= $name_field . ',';
            $query_parts['values'] .= $value_field . ',';
        }
        substr($query_parts['fields'], 0, -1);
        substr($query_parts['values'], 0, -1);
        return $query_parts;
    }
    
    public function save_site(origin $origin_db, destination $destination_db){
        $site = $origin_db->get_site();
        $part_query_add_site = $this->create_query($site);
        return $destination_db->add_entry($part_query_add_site, 'course');
    }
    
    public function save_course(int $id, origin $origin_db, destination $destination_db){
        $course = $origin_db->get_course($id);
        $part_query = $this->create_query($course);
        return $destination_db->add_entry($part_query, 'course');
    }

    public function save_attemps_quiz(array $attemp, destination $destination_db, utils $utils, asso $asso_db){
        $attemp = $this->change_user_ref($attemp, $utils, $asso_db);
        $part_query = $this->create_query($attemp);
        return $destination_db->add_entry($part_query, 'quiz_attempts');
    }

    public function save_quiz(int $id, origin $origin_db, destination $destination_db){
        $quiz = $origin_db->get_quiz($id);
        $part_query = $this->create_query($quiz);
        return $destination_db->add_entry($part_query, 'quiz');
    }

    public function save_course_module(int $id, origin $origin_db, destination $destination_db){
        if($id !== 0){
            if($destination_db->module_course_exist($id)){
                $course_module = $origin_db->get_course_module($id);
                $part_query = $this->create_query($course_module, 'course_modules');
                return $destination_db->add_entry($part_query);
            }
        }else{
            return true;
        }
    }

    public function save_quiz_slot(int $id, origin $origin_db, destination $destination_db){
        $quiz_slot = $origin_db->get_quiz_slot($id);
        $part_query = $this->create_query($quiz_slot);
        return $destination_db->add_entry($part_query, 'quiz_slots');
    }

    public function save_question(int $id, origin $origin_db, destination $destination_db, utils $utils, asso $asso_db){
        $question = $origin_db->get_question($id);
        $question = $this->change_user_ref($question, $utils, $asso_db);
        $part_query = $this->create_query($quiz_slot);
        return $destination_db->add_entry($part_query, 'question');
    }

    public function save_question_attemps(array $attemp, origin $origin_db, destination $destination_db){
        $part_query = $this->create_query($attemp);
        return $destination_db->add_entry($part_query, 'question_attempts');
    }

    public function save_question_usage(int $id, origin $origin_db, destination $destination_db){
        $question_usage = $origin_db->get_question_usage($id);
        $part_query = $this->create_query($question_usage);
        return $destination_db->add_entry($part_query, 'question_usages');
    }

    public function save_grade_items(array $grade_items, origin $origin_db, destination $destination_db){
        $part_query = $this->create_query($grade_items);
        return $destination_db->add_entry($part_query, 'grade_items');
    }
    
    public function save_grade_grade(int $id_user, int $id_grade_items, origin $origin_db, destination $destination_db, utils $utils, asso $asso_db){
        $grade_grade = $origin_db->get_grade_grades($id_user, $id_grade_items);
        $grade_grade = $this->change_user_ref($grade_grade, $utils, $asso_db);
        $part_query = $this->create_query($grade_grade);
        return $destination_db->add_entry($part_query, 'grade_grades');
    }

    public function save_module_in_course(int $id_course, origin $origin_db, destination $destination_db){
        $module_in_course = $origin_db->get_module_in_course($id_course);
        foreach ($module_in_course as $module) {
            $id_module = $module['module'];
            $id_instance = $module['instance'];
            $module_table = $origin_db->get_module_table_name($id_module);
            $module_instance = $origin_db->get_module_instance($id_instance, $module_table);
            $module_part_query = $this->create_query($module);
            $module_instance_part_query = $this->create_query($module_instance);
            $save_module = $destination_db->add_entry($module_part_query, 'course_modules');
            $save_module_instance = $destination_db->add_entry($module_part_query, $module_table);
        }
    }

    public function save_module_instance(int $id, string $table, origin $origin_db, destination $destination_db){
        $module = $origin_db->get_module_instance($id, $table);
        $module_query = $this->create_query($module);
        return $destination_db->add_entry($module_query, $table);
    }
}