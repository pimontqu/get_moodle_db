<?php
require_once 'config.php';
date_default_timezone_set('Europe/Paris');
$conf_ini = parse_ini_file('config.ini', true);
$asso_db = new asso($conf_ini['assos']);
$origin_db = new origin($conf_ini['origin']);
$destination_db = new destination($conf_ini['destination']);
$utils = new utils($conf_ini['hash']['key']);
$db_helper = new db_helper();
//write_logs('start');
$origin_users = $origin_db->get_users();
foreach ($origin_users as $user) {
    $hash_id_user = $utils->anonymous_id($user['id']);
    if(!$asso_db->check_id_exist($hash_id_user)){
        $user_anonymised = $utils->anonymous_user($user['email']);
        $user['email'] = $user_anonymised['mail'];
        $user['username'] = $user_anonymised['username'];
        $user['firstname'] = $user_anonymised['firstname'];
        $user['lastname'] = $user_anonymised['lastname'];
        $first_line_query = '';
        $scd_line_query = '';
        foreach ($user as $field_name => $field_value) {
            if(gettype($field_name) != 'integer' and $field_name != 'id'){

                $first_line_query .= $field_name . ',';
                $scd_line_query .= '"' . $field_value . '",';
            }
        }
        $destination_db->add_user($first_line_query, $scd_line_query);
        $last_id_user = $destination_db->last_insert_id();
        $asso_db->add_asso($hash_id_user, $last_id_user);
    }
}


$tables = array('course', 'logstore_standard_log');
$tables = array_merge($tables, $utils->add_module_table($origin_db));
$action_logs_valid = array('\core\event\user_loggedout', '\core\event\user_loggedin', '\mod_quiz\event\attempt_submitted', '\core\event\course_viewed',
'\mod_quiz\event\attempt_started', '\core\event\user_enrolment_created', '\core\event\user_created', '\mod_quiz\event\course_module_viewed');
$is_first_exe = $asso_db->is_first_exe();
$limit_logs = 0;

if($is_first_exe){
    /*
        supression des table dans la varibale $tables
    */
    $limit_logs = time() - 1728000;
    //$asso_db->make_first_exe();
}else{
    //$limit_logs = $asso_db->get_last_log();
}

$end_queries = str_replace('\\', '\\\\', 'eventname = "' . implode('" OR eventname = "', $action_logs_valid) . '"');
$logs = $origin_db->get_logs($limit_logs, $end_queries);

foreach ($logs as $log) {
    $event = $log['eventname'];
    $id_user = $log['userid'];
    $log = $db_helper->change_user_ref($log, $utils, $asso_db);
    if(in_array($event, $action_logs_valid)){
        $id_course_module = $log['contextinstanceid'];
        $db_helper->save_course_module($id_course_module, $origin_db, $destination_db);
        // connexion, deconnexion
        if($event == $action_logs_valid[0] || $event == $action_logs_valid[1]){
            // enregistre le site si il est pas déjà enregistré
            if(!$destination_db->check_site_exit()){
                $db_helper->save_site($origin_db, $destination_db);
            }
        // incription au cours
        }elseif ($event == $action_logs_valid[5]) {
            $id_course = $log['courseid'];
            if(!$destination_db->check_course_exit($id_course)){
                $db_helper->save_course($id_course, $origin_db, $destination_db);
                $db_helper->save_module_in_course($id_course, $origin_db, $destination_db);
            }
        // inscription à la plateforme
        }elseif($event == $action_logs_valid[6]){
            // enregistre le site si il est pas déjà enregistré
            if(!$destination_db->check_site_exit()){
                $db_helper->save_site($origin_db, $destination_db);
            }
        // visualisation cours
        }elseif ($event == $action_logs_valid[3]) {
            $id_course = $log['courseid'];
            if(!$destination_db->check_course_exit($id_course)){
                $db_helper->save_course($id_course, $origin_db, $destination_db);
                $db_helper->save_module_in_course($id_course, $origin_db, $destination_db);
            }
        // commencement d'un test
        }elseif($event == $action_logs_valid[4]){
            $id_attemp_quiz = $log['objectid'];
            $attemp_quiz = $origin_db->get_attemp_quiz($id_attemp_quiz);
            $id_quiz = $attemp_quiz['quiz'];
            if(!$destination_db->check_attemps_quiz_exit($id_attemp_quiz)){
                $db_helper->save_attemps_quiz($attemp_quiz, $origin_db, $destination_db, $utils, $asso_db);
            }
            if(!$destination_db->check_quiz_exist($id_quiz)){
                $db_helper->save_quiz($id_quiz, $origin_db, $destination_db);
            }
        // vue d'un module de cours 
        }elseif($event == $action_logs_valid[7]){
            $id_module_instance = $event['objectid'];
            $table_module = $event['target'];
            $course_id = $log['courseid'];
            if(!$destination_db->check_course_exit($course_id)){
                $db_helper->save_course($id_course, $origin_db, $destination_db);
                $db_helper->save_module_in_course($id_course, $origin_db, $destination_db);
            }
            if(!$destination_db->module_exist($id_module_instance, $table_module)){
                $db_helper->save_module_instance($id_module_instance, $table_module, $origin_db, $destination_db);
            }
        // finition d'un test
        }else{
            //partie quiz
            $id_attemp_quiz = $log['objectid'];
            $attemp_quiz = $origin_db->get_attemp_quiz($id_attemp_quiz);
            $id_quiz = $attemp_quiz['quiz'];
            $timefinished_quiz = $attemp_quiz[''];
            if(!$destination_db->check_attemps_quiz_exit($id_attemp_quiz)){
                $db_helper->save_attemps_quiz($attemp_quiz, $origin_db, $destination_db, $utils, $asso_db);
            }
            if(!$destination_db->check_quiz_exist($id_quiz)){
                $db_helper->save_quiz($id_quiz, $origin_db, $destination_db);
            }
            // partie score question
            $grade_items = $origin_db->get_grade_items($id_quiz);
            if(!$destination_db->grade_items_exist($id_quiz)){
                $db_helper->save_grade_items($grade_items, $origin_db, $destination_db);
            }
            $id_grade_items = $grade_items['id'];
            $db_helper->save_grade_grade($id_user, $id_grade_items, $origin_db, $destination_db, $utils, $asso_db);
            // partie question
            $quiz_slots = $origin_db->get_quiz_slot($id_quiz);
            foreach ($quiz_slots as $slot) {
                $id_slot = $slot['id'];
                if(!$destination_db->quiz_slot_exit($id)){
                    $db_helper->save_quiz_slot($id_slot, $origin_db, $destination_db);
                }
                $id_question = $slot['questionid'];
                if(!$destination_db->question_exist($id_question)){
                    $db_helper->save_question($id_question, $origin_db, $destination_db, $utils, $asso_db);
                }
            }
            // partie tentative question
            $unique_id = $attemp_quiz['uniqueid'];
            $db_helper->save_question_usage($unique_id, $origin_db, $destination_db);
            $questions_attemps = $origin_db->get_question_attemps($unique_id);
            foreach ($questions_attemps as $q_attemp) {
                $db_helper->save_question_attemps($q_attemp, $origin_db, $destination_db);
            }
            
        }
        $parts_log = $db_helper->create_query($log);
        $destination_db->add_entry($parts_log, 'logstore_standard_log');
    }
}
